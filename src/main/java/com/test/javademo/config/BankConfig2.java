package com.test.javademo.config;

import com.test.javademo.dto.Contact;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "bank")
public class BankConfig2 {
    private String name;

    private Long opened;

    private List<String> offices;

    private List<Contact> contacts;
}
