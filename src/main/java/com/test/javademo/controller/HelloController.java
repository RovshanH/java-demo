package com.test.javademo.controller;

import com.test.javademo.dto.StudentDto;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

@RestController(value = "myHelloController")
@RequestMapping("/")
public class HelloController {
    // http://localhost:8080/en
    @GetMapping("/en")
    public String sayHello(){
        return "Hello Spring";
    }

    // http://localhost:8080/en2
    @GetMapping("/en2")
    public String sayHello2(){
        return "Hello Spring2";
    }

    // http://localhost:8080/az
    @GetMapping("/az")
    public String sayHelloAz(){
        return "Salam Dunya";
    }

    // http://localhost:8080/age/2/size/10
    @GetMapping("/age/{age}/size/{size}")
    public String sayHelloAz2(@PathVariable Long age, @PathVariable Long size){
        return "Salam Dunya " + age + " " + size;
    }

    // http://localhost:8080/az3
    // public static final String ACCEPT_LANGUAGE = "Accept-Language";
    @GetMapping("/az3")
    public String sayHelloAz3(@RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) String lang){
        return "Salam Dunya " + lang;
    }

    // http://localhost:8080/az4
    // public static final String ACCEPT_LANGUAGE = "Accept-Language";
    @GetMapping("/az4")
    public String sayHelloAz4(@RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) String lang,
                              @RequestHeader("course") String course){
        return "Salam Dunya " + lang + " " + course;
    }

    // http://localhost:8080/en3?name=Azad
    @GetMapping("/en3")
    public String sayHelloEn3(@RequestParam("name") String studentName){
        return "Hello " + studentName;
    }

    // http://localhost:8080/en4?name=Azad&lastName=Azadov
    @GetMapping("/en4")
    public String sayHelloEn4(@RequestParam("name") String studentName,
                              @RequestParam String lastName){
        return "Hello " + studentName + " " + lastName;
    }

    // http://localhost:8080/en5?firstName=Azad&lastName=Azadov
    @GetMapping("/en5")
    public String sayHelloEn5(StudentDto student){
        return "Hello " + student;
    }
}
