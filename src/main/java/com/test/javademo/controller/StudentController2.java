package com.test.javademo.controller;

import com.test.javademo.dto.StudentRequestDto;
import com.test.javademo.dto.StudentResponseDto;
import com.test.javademo.entity.Student;
import com.test.javademo.service.StudentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/students2")
public class StudentController2 {

    private final StudentService studentService;

    public StudentController2(@Qualifier("service1") StudentService studentService) {
        this.studentService = studentService;
    }

    // http://localhost:9090/students2/1
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Student getStudent(@PathVariable Long id){
         return studentService.getStudentById(id);
    }

    // http://localhost:9090/students2/list
    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public List<Student> getStudents(){
        return studentService.getStudentList();
    }

    // http://localhost:9090/students2/
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StudentResponseDto createStudent(@Valid @RequestBody StudentRequestDto student){
        return studentService.createStudent(student);
    }

    // http://localhost:9090/students2/15
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Student updateStudent(@PathVariable Long id, @RequestBody Student student){
        return studentService.updateStudent(id, student);
    }

    // http://localhost:9090/students2/15
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Long id){
        studentService.deleteStudent(id);
    }
}
