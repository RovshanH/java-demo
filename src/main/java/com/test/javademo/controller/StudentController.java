package com.test.javademo.controller;

import com.test.javademo.dto.StudentDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/students")
public class StudentController {
    // http://localhost:8080/students/1
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentDto getStudent(@PathVariable Long id){
        StudentDto studentDto = new StudentDto(1L,"Rovshan", "Huseynov");
        System.out.println("get student " + studentDto);
        return studentDto;
    }

    // http://localhost:8080/students/list
    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public List<StudentDto> getStudents(){
        List<StudentDto> studentDtoList = new ArrayList<>();
        IntStream.rangeClosed(1,10).forEach(i -> studentDtoList.add(new StudentDto((long) i, "Azer", "Azerov"+i)));
        return studentDtoList;
    }

    // http://localhost:8080/students
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto createStudent(@RequestBody StudentDto studentDto){
        System.out.println(studentDto);
        return studentDto;
    }

    // http://localhost:8080/students/1
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentDto updateStudent(@PathVariable Long id, @RequestBody StudentDto studentDto){
        System.out.println("update student with id " + id + " to " + studentDto);
        return studentDto;
    }

    // http://localhost:8080/students/1
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable Long id){
        System.out.println("delete student with id " + id);
    }
}
