package com.test.javademo.service;

public final class Singleton {
    private static Singleton singleton;
    private Singleton(){}

    // option 1 - not good because it works so late. Go get instance 1 million times
    // we must use synchronized 1 million times
    public synchronized static Singleton getInstance(){
        if(singleton == null){
            singleton = new Singleton();
        }
        return singleton;
    }

    // option 2 - Synchronized block
    public static Singleton getInstance2(){
        if(singleton == null) {
            synchronized (Singleton.class) {
                if(singleton == null)
                    singleton = new Singleton();
            }
        }
        return singleton;
    }
}
