package com.test.javademo.service;

import com.test.javademo.dto.StudentRequestDto;
import com.test.javademo.dto.StudentResponseDto;
import com.test.javademo.entity.Student;
import com.test.javademo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("service1")
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    public Student getStudentById(Long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
    }

    @Override
    public StudentResponseDto createStudent(StudentRequestDto studentDto) {
        Student student = modelMapper.map(studentDto, Student.class);
        Student studentResponse = studentRepository.save(student);
        return modelMapper.map(studentResponse, StudentResponseDto.class);
    }

    @Override
    public Student updateStudent(Long id, Student student) {
        checkIfExists(id);
        student.setId(id);
        return studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Long id) {
        checkIfExists(id);
        studentRepository.deleteById(id);
    }

    @Override
    public List<Student> getStudentList() {
        return studentRepository.findAll();
    }

    private void checkIfExists(Long id){
        studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
    }
}
