package com.test.javademo.service;

import com.test.javademo.dto.StudentRequestDto;
import com.test.javademo.dto.StudentResponseDto;
import com.test.javademo.entity.Student;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

@Primary
@Component
public class StudentServiceImpl2 implements StudentService{
    @Override
    public StudentResponseDto createStudent(StudentRequestDto student) {
        return null;
    }

    @Override
    public Student updateStudent(Long id, Student student) {
        return null;
    }

    @Override
    public void deleteStudent(Long id) {

    }

    @Override
    public List<Student> getStudentList() {
        return null;
    }

    @Override
    public Student getStudentById(Long id) {
        return null;
    }
}
