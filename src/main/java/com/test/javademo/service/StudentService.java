package com.test.javademo.service;

import com.test.javademo.dto.StudentRequestDto;
import com.test.javademo.dto.StudentResponseDto;
import com.test.javademo.entity.Student;

import java.util.List;

public interface StudentService {
    StudentResponseDto createStudent(StudentRequestDto student);
    Student updateStudent(Long id, Student student);
    void deleteStudent(Long id);
    List<Student> getStudentList();
    Student getStudentById(Long id);
}
