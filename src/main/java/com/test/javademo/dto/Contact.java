package com.test.javademo.dto;

import lombok.Data;

@Data
public class Contact {
    private String phone;
    private String email;
}
