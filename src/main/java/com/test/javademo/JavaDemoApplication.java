package com.test.javademo;

import com.test.javademo.config.BankConfig;
import com.test.javademo.config.BankConfig2;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class JavaDemoApplication implements CommandLineRunner {
	private final BankConfig bankConfig;
	private final BankConfig2 bankConfig2;
	public static void main(String[] args) {
		SpringApplication.run(JavaDemoApplication.class, args);
		System.out.println("Ended");
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Spring now started");
		System.out.println("bankConfig: " + bankConfig);
		System.out.println("bankConfig2: " + bankConfig2);
	}
}
