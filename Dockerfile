FROM alpine:3.14
RUN apk add --no-cache openjdk11
COPY build/libs/jar_file_name.jar /app/
CMD java -jar /app/jar_file_name.jar